# Project Init

## DB

- Install and launch [docker-compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04)
- cd in the app directory
- launch ```docker-compose up -d``` in your terminal

## Dependencies 

- Run ```composer install```

## Symfony server

- Install [Symfony CLI](https://symfony.com/download)
- run ```symfony serve -d```
- Follow the link prompted in your terminal

## Fixture
Fixtures are based on the remote json file
- run ```symfony console doctrine:fixtures:load```

Et voilà ! (enfin j'éspère)
