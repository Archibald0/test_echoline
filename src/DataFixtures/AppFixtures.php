<?php

namespace App\DataFixtures;

use App\Entity\Film;
use App\Entity\FilmHasPerson;
use App\Entity\Person;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AppFixtures extends Fixture
{
    private $httpClient;

    private $passwordHasher;

    public function __construct(HttpClientInterface $httpClient, UserPasswordHasherInterface $passwordHasher)
    {
        $this->httpClient = $httpClient;
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $response = $this->httpClient->request('GET', 'https://developer.echoline.fr/surprise-me');
        $content = $response->getContent();
        $datas = json_decode($content, true);
        // pour alléger le traitement
        $personCreateds = [];
        $filmCreateds = [];
        foreach($datas['films'] as $filmDatas) {
            $film = new Film();
            $film->setTitle($filmDatas['title']);
            $film->setDuration($filmDatas['duration']);
            $film->setPressRating($filmDatas['ratings']['press'] ?? null);
            $film->setPublicRating($filmDatas['ratings']['public'] ?? null);
            $manager->persist($film);
            $filmCreateds[$filmDatas['id']] = $film;

            foreach($filmDatas['main_actors'] as $actor) {
                if(!isset($personCreateds[$actor])) {
                    $person = new Person();
                    $person->setName($actor);
                    $manager->persist($person);
                    $personCreateds[$actor] = $person;
                } else {
                    $person = $personCreateds[$actor];
                }
                $filmHasPerson = new FilmHasPerson();
                $filmHasPerson->setFilm($film);
                $filmHasPerson->setPerson($person);
                $filmHasPerson->setType(FilmHasPerson::TYPE_ACTOR);
                $manager->persist($filmHasPerson);
            }

            foreach($filmDatas['directors'] as $actor) {
                if(!isset($personCreateds[$actor])) {
                    $person = new Person();
                    $person->setName($actor);
                    $manager->persist($person);
                    $personCreateds[$actor] = $person;
                } else {
                    $person = $personCreateds[$actor];
                }
                $filmHasPerson = new FilmHasPerson();
                $filmHasPerson->setFilm($film);
                $filmHasPerson->setPerson($person);
                $filmHasPerson->setType(FilmHasPerson::TYPE_DIRECTOR);
                $manager->persist($filmHasPerson);
            }
        }

        foreach($datas['users'] as $userData) {
            $user = new User();
            $user->setEmail($userData['email']);
            $user->setFirstname($userData['firstname']);
            $user->setLastname($userData['lastname']);
            // par facilité, on se servira ici de l'id pour générer le mot de passe
            $hashedPassword = $this->passwordHasher->hashPassword(
                $user,
                $userData['id']
            );
            $user->setPassword($hashedPassword);

            foreach($userData['favourite_films'] as $favoriteFilm) {
                $user->addFilm($filmCreateds[$favoriteFilm]);
            }
            $manager->persist($user);
        }
        $manager->flush();
    }
}
