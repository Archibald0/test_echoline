<?php

namespace App\Entity;

use App\Repository\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PersonRepository::class)]
class Person
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'person', targetEntity: FilmHasPerson::class)]
    private Collection $films;

    public function __construct()
    {
        $this->films = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, FilmHasPerson>
     */
    public function getFilms(): Collection
    {
        return $this->films;
    }

    public function addFilm(FilmHasPerson $film): self
    {
        if (!$this->films->contains($film)) {
            $this->films->add($film);
            $film->setPerson($this);
        }

        return $this;
    }

    public function removeFilm(FilmHasPerson $film): self
    {
        if ($this->films->removeElement($film)) {
            // set the owning side to null (unless already changed)
            if ($film->getPerson() === $this) {
                $film->setPerson(null);
            }
        }

        return $this;
    }
}
