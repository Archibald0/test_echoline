<?php

namespace App\Entity;

use App\Repository\FilmRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FilmRepository::class)]
class Film
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column]
    private ?int $duration = null;

    #[ORM\Column(nullable: true)]
    private ?float $public_rating = null;

    #[ORM\Column(nullable: true)]
    private ?float $press_rating = null;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'films')]
    private Collection $users;

    #[ORM\OneToMany(mappedBy: 'film', targetEntity: FilmHasPerson::class, orphanRemoval: true)]
    private Collection $persons;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->persons = new ArrayCollection();
    }
    public function __toString(): string
    {
        return $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getPublicRating(): ?float
    {
        return $this->public_rating;
    }

    public function setPublicRating(?float $public_rating): self
    {
        $this->public_rating = $public_rating;

        return $this;
    }

    public function getPressRating(): ?float
    {
        return $this->press_rating;
    }

    public function setPressRating(?float $press_rating): self
    {
        $this->press_rating = $press_rating;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    /**
     * @return Collection<int, FilmHasPerson>
     */
    public function getPersons(): Collection
    {
        return $this->persons;
    }

    public function addPerson(FilmHasPerson $person): self
    {
        if (!$this->persons->contains($person)) {
            $this->persons->add($person);
            $person->setFilm($this);
        }

        return $this;
    }

    public function removePerson(FilmHasPerson $person): self
    {
        if ($this->persons->removeElement($person)) {
            // set the owning side to null (unless already changed)
            if ($person->getFilm() === $this) {
                $person->setFilm(null);
            }
        }

        return $this;
    }
}
